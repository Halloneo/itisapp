﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Authorization.Requirements;
using Microsoft.AspNetCore.Authorization;

namespace ITISApp.Domain.Core.Authorization.Handlers
{
    public class RoleHandler : AuthorizationHandler<RoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleRequirement requirement)
        {
            var roles = context.User.Claims.Where(x => x.Type == ClaimTypes.Role);

            if (roles.Any(x => x.Value == requirement.Code))
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}