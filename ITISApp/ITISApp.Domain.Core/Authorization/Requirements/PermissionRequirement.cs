﻿using Microsoft.AspNetCore.Authorization;

namespace ITISApp.Domain.Core.Authorization.Requirements
{
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public string Code { get; }

        public PermissionRequirement(string permissionCode)
        {
            Code = permissionCode;
        }
    }
}