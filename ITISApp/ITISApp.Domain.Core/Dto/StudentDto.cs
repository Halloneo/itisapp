﻿using ITISApp.Domain.Core.Entities;
using System.Collections.Generic;

namespace ITISApp.Domain.Core.Dto
{
    public class StudentDto : UserInfoDto
    {
        public StudentDto(Student student, User user, List<string> permissionCodes)
            : base(user, permissionCodes)
        {
            StudentId = student.Id;
            AcademicGroup = student.AcademicGroup;
            CourseNumber = student.AcademicGroup.CourseNumber;
            IsStudent = true;
        }

        public int StudentId { get; set; }
        
        public bool IsStudent { get; set; }

        public AcademicGroup AcademicGroup { get; set; }

        public int CourseNumber { get; set; }
    }
}