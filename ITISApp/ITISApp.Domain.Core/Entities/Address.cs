﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    public class Address : BaseEntity
    {
        public string City { get; set; }

        public string Street { get; set; }

        /// <summary>
        /// Номер дома (вместе с буквой или номером корпуса).
        /// </summary>
        public string House { get; set; }

        public string Description { get; set; }
    }
}