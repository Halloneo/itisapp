﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.Authorization
{
    public class UserRole : BaseEntity
    {
        public virtual User User { get; set; }

        public virtual Role Role { get; set; }
    }
}