﻿namespace ITISApp.Domain.Core.Entities.Base
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}