﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Text;
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    public class FileInfo : BaseEntity
    {
        /// <summary> имя файла с расширением </summary>
        public string FullName { get; set; }

        /// <summary> Название файла без расшширения </summary>
        public string Name { get; set; }

        /// <summary> Расширение файла </summary>
        public string Extension { get; set; }

        /// <summary> Тип передаваемого контента </summary>
        public string ContentType { get; set; }

        /// <summary> Путь к файлу </summary>
        public string Path { get; set; }
        
        ///// <summary> Навигационное свойство на пару </summary>
        //public virtual Lesson Lesson { get; set; }

        /// <summary> Дата загрузки </summary>
        public DateTime? CreatedTime { get; set; }
    }
}
