﻿using System;
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    /// <summary>
    /// Занятие.
    /// </summary>
    public class Lesson : BaseEntity
    {
        /// <summary>
        /// Дата и время проведения занятия.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Адрес здания, где проводится занятие.
        /// </summary>
        public virtual Address Address { get; set; }

        /// <summary>
        /// Номер аудитории.
        /// </summary>
        public string RoomNumber { get; set; }

        public virtual TimetableInfo TimetableInfo { get; set; }
    }
}