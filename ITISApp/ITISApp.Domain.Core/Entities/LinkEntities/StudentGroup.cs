﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.LinkEntities
{
    /// <summary>
    /// Сущность-связка между группой и входящими в нее студентами.
    /// </summary>
    public class StudentGroup : BaseEntity
    {
        public virtual Student Student { get; set; }

        public virtual OptionalGroup OptionalGroup { get; set; }
    }
}