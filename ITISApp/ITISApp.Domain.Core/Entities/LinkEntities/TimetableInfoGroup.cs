﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.LinkEntities
{
    /// <summary>
    /// Сущность-связка между предметом и группами, которые должны его посещать.
    /// </summary>
    public class TimetableInfoGroup : BaseEntity
    {
        public virtual TimetableInfo TimetableInfo { get; set; }

        public virtual AcademicGroup AcademicGroup { get; set; }
    }
}