﻿using ITISApp.Domain.Core.Entities.Base;
using ITISApp.Domain.Core.Entities.LinkEntities;

namespace ITISApp.Domain.Core.Entities
{
    /// <summary>
    /// Группа студентов по курсу по выбору/предмету с разделением на подгруппы.
    /// </summary>
    public class OptionalGroup : BaseEntity
    {
        /// <summary>
        /// Номер подгруппы в рамках одного курсу по выбору/предмету с разделением на подгруппы;
        /// Если нет дополнительного разделения, тогда null.
        /// </summary>
        public int? SubGroupNumber { get; set; }

        public virtual SubjectTeacher SubjectTeacher { get; set; }
    }
}