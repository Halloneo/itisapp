﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    public class Student : BaseEntity
    {
        public virtual User User { get; set; }

        public virtual AcademicGroup AcademicGroup { get; set; }
    }
}