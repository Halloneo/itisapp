﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    /// <summary>
    /// Предмет.
    /// </summary>
    public class Subject : BaseEntity
    {
        /// <summary>
        /// Название предмета.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер курса.
        /// </summary>
        public int CourseNumber { get; set; }
        
        /// <summary>
        /// Является ли предмет курсом по выбору.
        /// </summary>
        public bool IsOptional { get; set; }

        public virtual OptionalCourseSection OptionalCourseSection { get; set; }
    }
}