﻿using System;
using ITISApp.Domain.Core.Entities.Base;
using ITISApp.Domain.Core.Entities.LinkEntities;
using ITISApp.Domain.Core.Interfaces;

namespace ITISApp.Domain.Core.Entities
{
    public class TimetableInfo : BaseEntity, IHaveSemester
    {
        public virtual SubjectTeacher SubjectTeacher { get; set; }

        /// <summary>
        /// Ссылка на группу по данному предмету (пустая, если нет разделения на подгруппы).
        /// </summary>
        public virtual OptionalGroup OptionalGroup { get; set; }

        /// <summary>
        /// Текущий семестр.
        /// </summary>
        public virtual Semester Semester { get; set; }

        /// <summary>
        /// Номер аудитории.
        /// </summary>
        public string RoomNumber { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public TimeSpan Time { get; set; }
    }
}