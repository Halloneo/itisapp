﻿using ITISApp.Domain.Core.Common.Exceptions;
using ITISApp.Domain.Core.Common.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ITISApp.Domain.Core.Filters
{
    public class AuthorizationExceptionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is AuthorizationException)
            {
                context.Result = new OkObjectResult(new OperationResult
                {
                    Success = false,
                    Message = context.Exception.Message
                });

                context.ExceptionHandled = true;
                context.Exception = null;
            }

            base.OnActionExecuted(context);
        }
    }
}