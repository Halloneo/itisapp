﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Common;
using ITISApp.Domain.Core.Entities.Base;
using ITISApp.Domain.Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ITISApp.Domain.Core.Services
{
    public class DataStore : IDataStore
    {
        private readonly DbContext _dbContext;

        public DataStore(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public TEntity Get<TEntity>(int entityId)
            where TEntity : BaseEntity
        {
            return _dbContext.Find<TEntity>(entityId);
        }

        public async Task<TEntity> GetAsync<TEntity>(int entityId)
            where TEntity : BaseEntity
        {
            return await _dbContext.FindAsync<TEntity>(entityId);
        }

        public IQueryable<TEntity> GetAll<TEntity>()
            where TEntity : BaseEntity
        {
            return _dbContext.Set<TEntity>().AsQueryable();
        }

        public TEntity Create<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Add(entity);
            return entity;
        }
        
        public TEntity CreateAndSave<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public async Task<TEntity> CreateAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            await _dbContext.AddAsync(entity);
            return entity;
        }
        
        public async Task<TEntity> CreateAndSaveAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            await  _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public TEntity Update<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Update(entity);
            return entity;
        }
        
        public TEntity UpdateAndSave<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Update(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public async Task<TEntity> UpdateAndSaveAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Update(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public void Delete<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Remove(entity);
        }
        
        public void DeleteAndSave<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Remove(entity);
            _dbContext.SaveChanges();
        }

        public async Task DeleteAndSaveAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity
        {
            _dbContext.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
        
        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void InTransaction(Action action)
        {
            try
            {
                action();
                SaveChanges();
            }
            catch
            {
                throw new ValidationException("Ошибка транзакции");
            }
        }
        
        public async Task InTransactionAsync(Func<Task> action)
        {
            try
            {
                await action();
                await SaveChangesAsync();
            }
            catch
            {
                throw new ValidationException("Ошибка транзакции");
            }
        }
    }
}