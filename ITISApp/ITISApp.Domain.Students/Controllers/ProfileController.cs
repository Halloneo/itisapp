﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Common.Results;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.Authorization;
using ITISApp.Domain.Core.Extensions;
using ITISApp.Domain.Core.Helpers;
using ITISApp.Domain.Core.Interfaces;
using ITISApp.Domain.Students.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ITISApp.Domain.Students.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class ProfileController : Controller
    {
        private readonly IDataStore _dataStore;

        public ProfileController(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        [HttpGet]
        public async Task<OperationResult> GetAcademicGroups(int courseNumber)
        {
            var groups = await _dataStore.GetAll<AcademicGroup>()
                .Where(x => x.CourseNumber == courseNumber)
                .Select(x => new
                {
                    x.Id,
                    x.Name
                })
                .OrderBy(x => x.Name)
                .ToListAsync();

            return new OperationResult
            {
                Success = true,
                Data = groups
            };
        }

        [HttpPost]
        public async Task<OperationResult> CreateStudent([FromBody] StudentEditDto editDto)
        {
            var user = await _dataStore.GetAsync<User>(editDto.Id);

            var academicGroup = await _dataStore.GetAsync<AcademicGroup>(editDto.AcademicGroup.Id);
            var student = new Student
            {
                User = user,
                AcademicGroup = academicGroup
            };
            await _dataStore.CreateAndSaveAsync(student);

            var studentRole = _dataStore.GetAll<Role>().FirstOrDefault(x => x.Code == RoleCodesHelper.Student);
            var userRole = new UserRole
            {
                User = user,
                Role = studentRole
            };
            await _dataStore.CreateAndSaveAsync(userRole);

            return new OperationResult
            {
                Success = true
            };
        }

        [HttpPost]
        public async Task<OperationResult> UpdateStudent([FromBody] StudentEditDto editDto)
        {
            var login = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            var currentUser = _dataStore.GetAll<User>().FirstOrDefault(x => x.Login == login);

            if (currentUser?.Id != editDto.Id && !User.IsAdmin())
            {
                return new OperationResult(false, "У вас нет прав изменять данные других пользователей");
            }

            var user = await _dataStore.GetAsync<User>(editDto.Id);
            var student = _dataStore.GetAll<Student>().FirstOrDefault(x => x.User.Id == user.Id);

            await UpdateStudent(student, editDto);
            await _dataStore.UpdateAndSaveAsync(student);

            return new OperationResult
            {
                Success = true
            };
        }

        [HttpPost]
        public async Task<OperationResult> CreateTeacher([FromBody] TeacherEditDto editDto)
        {
            var user = await _dataStore.GetAsync<User>(editDto.Id);

            var teacherName = $"{editDto.Surname} {editDto.Name.FirstOrDefault()}.";
            var teacherFullName = teacherName + $"{editDto.MiddleName?.FirstOrDefault()}.";

            Teacher existingTeacher;
            var teachers = await _dataStore
                .GetAll<Teacher>()
                .Where(x => x.TeacherName.Contains(teacherName))
                .ToListAsync();

            if (teachers?.Count == 1)
            {
                existingTeacher = teachers.FirstOrDefault();
            }
            else
            {
                existingTeacher = _dataStore
                    .GetAll<Teacher>()
                    .FirstOrDefault(x => x.TeacherName.Contains(teacherFullName));
            }

            if (existingTeacher != null)
            {
                if (existingTeacher.User != null)
                {
                    return new OperationResult(false,
                        "Уже имеется преподаватель с такими ФИО, обратитесь к администратору");
                }

                UpdateTeacher(existingTeacher, editDto, user);
                await _dataStore.UpdateAndSaveAsync(existingTeacher);
            }
            else
            {
                existingTeacher = new Teacher();
                UpdateTeacher(existingTeacher, editDto, user);
                await _dataStore.CreateAndSaveAsync(existingTeacher);
            }

            var teacherRole = _dataStore.GetAll<Role>().FirstOrDefault(x => x.Code == RoleCodesHelper.Teacher);
            var userRole = new UserRole
            {
                User = user,
                Role = teacherRole
            };
            await _dataStore.CreateAndSaveAsync(userRole);

            return new OperationResult
            {
                Success = true
            };
        }

        [HttpPost]
        public async Task<OperationResult> UpdateTeacher([FromBody] TeacherEditDto editDto)
        {
            var user = await _dataStore.GetAsync<User>(editDto.Id);
            var teacher = _dataStore.GetAll<Teacher>().FirstOrDefault(x => x.User.Id == user.Id);

            UpdateUser(user, editDto);
            await _dataStore.UpdateAndSaveAsync(user);

            UpdateTeacher(teacher, editDto);
            await _dataStore.UpdateAndSaveAsync(teacher);

            return new OperationResult
            {
                Success = true
            };
        }

        private async Task UpdateStudent(Student student, StudentEditDto editDto)
        {
            student.AcademicGroup = await _dataStore.GetAsync<AcademicGroup>(editDto.AcademicGroup.Id);
        }

        private void UpdateTeacher(Teacher teacher, TeacherEditDto editDto, User user = null)
        {
            var nameFirstLetter = !string.IsNullOrEmpty(editDto.Name)
                ? editDto.Name.Substring(0, 1)
                : string.Empty;
            var editedTeacherName = $"{editDto.Surname} {nameFirstLetter}.";
            if (!string.IsNullOrEmpty(editDto.MiddleName))
            {
                editedTeacherName += $"{editDto.MiddleName.Substring(0, 1)}.";
            }

            if (user != null)
            {
                teacher.User = user;
            }

            teacher.TeacherName = editedTeacherName;
            teacher.WorkingExperience = editDto.WorkingExperience;
        }

        private void UpdateUser(User user, UserEditDto editDto)
        {
            user.Name = editDto.Name;
            user.Surname = editDto.Surname;
            user.MiddleName = editDto.MiddleName;
            user.ImageUrl = editDto.ImageUrl;
            user.Email = editDto.Email;
        }
    }
}