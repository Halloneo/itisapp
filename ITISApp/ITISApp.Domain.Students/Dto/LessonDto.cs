﻿using System;
using System.Linq.Expressions;
using ITISApp.Domain.Core.Entities;

namespace ITISApp.Domain.Students.Dto
{
    public class LessonDto
    {
        public string Title { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public int Duration { get; set; }

        public string RoomNumber { get; set; }

        public string TeacherName { get; set; }

        public string Groups { get; set; }
    }
}