﻿namespace ITISApp.Domain.Students.Dto
{
    public class OptionalCourseSelectionDto
    {
        public int SubjectId { get; set; }

        public bool IsSubject { get; set; }

        public int SectionId { get; set; }

        public string Name { get; set; }

        public bool HasSelectedGroup { get; set; }

        public string SelectedCourseName { get; set; }

        public string SelectedCourseTeacherName { get; set; }
        
        public string SelectedSubgroupNumber { get; set; }
    }
}