﻿namespace ITISApp.Domain.Students.Dto
{
    public class TeacherEditDto : UserEditDto
    {
        public int WorkingExperience { get; set; }
    }
}