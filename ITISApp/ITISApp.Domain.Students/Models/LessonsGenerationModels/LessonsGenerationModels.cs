﻿using System;
using System.Collections.Generic;

namespace ITISApp.Domain.Students.Models.LessonsGenerationModels
{
    public class TimeTableLessonInfoGenerationModel
    {
        public SubjectTeacherModel SubjectTeacherModel { get; set; }

        public LessonGenerationSettingsModel LessonGenerationSettingsModel { get; set; }

        public List<AcademicGroupModel> AcademicGroupModels { get; set; }

        public OptionalGroupModel OptionalGroupModel { get; set; }

        public Tuple<DateTime, bool> FirstLessonDateTime { get; set; }

        public string RoomNumber { get; set; }

        public TimeTableLessonInfoGenerationModel(
            SubjectTeacherModel subjectTeacherModel,
            AcademicGroupModel academicGroupModel,
            OptionalGroupModel optionalGroupModel,
            LessonGenerationSettingsModel lessonGenerationSettingsModel,
            Tuple<DateTime, bool> firstLessonDateTime,
            string roomNumber)
        {
            SubjectTeacherModel = subjectTeacherModel;
            AcademicGroupModels = new List<AcademicGroupModel>()
            {
                academicGroupModel
            };
            OptionalGroupModel = optionalGroupModel;
            LessonGenerationSettingsModel = lessonGenerationSettingsModel;
            FirstLessonDateTime = firstLessonDateTime;
            RoomNumber = roomNumber;
        }
    }

    public class SubjectTeacherModel
    {
        public SubjectModel SubjectModel { get; set; }

        public TeacherModel TeacherModel { get; set; }

        public SubjectTeacherModel(SubjectModel subjectModel, TeacherModel teacherModel)
        {
            SubjectModel = subjectModel;
            TeacherModel = teacherModel;
        }

    } 

    public class TeacherModel
    {
        public string Name { get; set; }

        public TeacherModel(string name)
        {
            Name = name;
        }
    }

    public class SubjectModel
    {
        public int CourseNumber { get; set; }

        public string Name { get; set; }

        public bool IsOptional { get; set; }

        public SubjectModel(int courseNumber, string name, bool isOptional)
        {
            CourseNumber = courseNumber;
            Name = name;
            IsOptional = isOptional;
        }
    }

    public class LessonGenerationSettingsModel
    {
        /// <summary>
        /// Разделение по четным и нечетным неделям
        /// true - по нечетным,
        /// false - по четным,
        /// null - нет разделения.
        /// </summary>
        public bool? IsOddOrEvenWeeksOnly { get; set; }

        public int? FromWeek { get; set; }

        public int? ToWeek { get; set; }

        public int? LessonsCount { get; set; }
    }

    public class AcademicGroupModel
    {
        public string Name { get; set; }

        public int CourseNumber { get; set; }

        public AcademicGroupModel(string name)
        {
            Name = name;
        }
    }

    public class OptionalGroupModel 
    {
        public SubjectTeacherModel SubjectTeacherModel { get; set; }

        public int? SubGroupNumber { get; set; }

        public OptionalGroupModel(SubjectTeacherModel subjectTeacherModel, int? subGroupNumber)
        {
            SubjectTeacherModel = subjectTeacherModel;
            SubGroupNumber = subGroupNumber;
        }
    }

    public class ParsingModel
    {
        public string SubjectName { get; set; }

        public string TeacherName { get; set; }

        public string RoomNumber { get; set; }

        public int? SubGroupNumber { get; set; }

        public LessonGenerationSettingsModel LessonGenerationSettingsModel { get; set; }

        public bool IsSubjectOptional { get; set; } = false;
    }

    public enum ParsingType
    {
        Normal,
        UnitedData,
        LessonWithoutTeacher
    }
}
