﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITISApp.Domain.Teachers.Models
{
    /// <summary> Настройки </summary>
    public class DomainOptions
    {
        /// <summary> Папка для файлов </summary>
        public string FileFolder { get; set; }

        /// <summary> Имя хоста </summary>
        public string HostName { get; set; }
    }
}
