﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Text;
using ITISApp.Domain.Core.Entities;

namespace ITISApp.Domain.Teachers.Models
{
    /// <summary> Модель представления для файла </summary>
    public class FileData
    {
        /// <summary> Данные файла </summary>
        public byte[] Data { get; set; }

        /// <summary> Название файла </summary>
        public string FileName { get; set; }

        /// <summary> Тип передаваемого ресурса </summary>
        public string ContentType { get; set; }

        ///// <summary> Пара, куда крепится файл </summary>
        //public Lesson Lesson { get; set; }
    }
}
