﻿using System;
using System.Collections.Generic;
using System.Text;
using ITISApp.Domain.Core.Entities;

namespace ITISApp.Domain.Teachers.Models
{
    public class FileInfo
    {
        /// <summary> имя файла с расширением </summary>
        public string FullName { get; set; }

        /// <summary> Путь файла, относительно корневой папки </summary>
        public string Path { get; set; }

        /// <summary> Ссылка на получение файла </summary>
        public string Url { get; set; }

        /// <summary> Тип передаваемого контента </summary>
        public string ContentType { get; set; }

        ///// <summary> Навигационное свойство на пару </summary>
        //public virtual Lesson Lesson { get; set; }

        /// <summary> Дата создания </summary>
        public DateTime CreatedTime { get; set; }
    }
}
