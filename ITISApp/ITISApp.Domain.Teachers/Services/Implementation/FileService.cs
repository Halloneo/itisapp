﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ITISApp.Domain.Core.Interfaces;
using ITISApp.Domain.Teachers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using FileInfo = ITISApp.Domain.Core.Entities.FileInfo;

namespace ITISApp.Domain.Teachers.Services.Implementation
{
    /// <inheritdoc cref="IFileService"/>
    public class FileService : IFileService
    {
        /// <summary> Бд </summary>
        private readonly IDataStore _dataStore;

        /// <summary> Маппер </summary>
        private readonly IMapper _mapper;

        /// <summary> Настройки </summary>
        private readonly IOptions<DomainOptions> _options;

        public FileService(IDataStore dataStore, IMapper mapper, IOptions<DomainOptions> options)
        {
            _dataStore = dataStore;
            _mapper = mapper;
            _options = options;
        }

        /// <inheritdoc cref="IFileService.Upload(FileData)"/>
        public async Task<FileInfo> Upload(FileData file)
        {
            var physicalFilePath = GetNewFilePath();

            using (var writer = new FileStream(physicalFilePath, FileMode.Create, FileAccess.Write))
            {
                await writer.WriteAsync(file.Data);
                await writer.FlushAsync();
            }

            var data = _mapper.Map<FileInfo>(file);

            data.Extension = Path.GetExtension(file.FileName);
            data.Name = Path.GetFileNameWithoutExtension(file.FileName);
            data.Path = physicalFilePath;
            data.CreatedTime = DateTime.Now;

            return await _dataStore.CreateAndSaveAsync(data);
        }

        /// <summary> Получение пути до файла </summary>
        private string GetNewFilePath()
        {
            var folderPath = _options.Value.FileFolder;

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var fileName = CombinePath(folderPath);


            return fileName;
        }

        private string CombinePath(string folderPath)
        {
            var fileName = Path.Combine(folderPath, Guid.NewGuid().ToString());
            return fileName.Replace('\\', '/');
        }

        /// <inheritdoc />
        public async Task<FileData> Download(string resource)
        {
            var fileMeta = _dataStore.GetAll<FileInfo>().AsNoTracking().FirstOrDefault(x => x.Path == resource);

            if (!File.Exists(resource))
            {
                throw new Exception($"Файл не найден по пути: {resource}");
            }

            return new FileData
            {
                //Lesson = fileMeta?.Lesson,
                ContentType = fileMeta?.ContentType,
                FileName = fileMeta?.FullName,
                Data = await File.ReadAllBytesAsync(resource)
            };
        }

        /// <inheritdoc cref="IFileService.Delete(string)"/>
        public async Task<FileInfo> Delete(string resource)
        {
            var fileMeta = _dataStore.GetAll<FileInfo>().FirstOrDefault(x => x.Path == resource);
            return await Delete(fileMeta);
        }

        /// <inheritdoc cref="IFileService.Delete(int)"/>
        public async Task<FileInfo> Delete(int id)
        {
            var fileMeta = _dataStore.Get<FileInfo>(id);
            if (fileMeta == null) throw new Exception("Информации о файле с указанным идентификатором не найдено.");

            return await Delete(fileMeta);
        }

        /// <summary> Удаление файла по классу </summary>
        private async Task<FileInfo> Delete(FileInfo fileMeta)
        {
            if (!File.Exists(fileMeta.Path))
            {
                throw new Exception($"Файл не найден по пути: {fileMeta.Path}");
            }

            await _dataStore.DeleteAndSaveAsync(fileMeta);

            File.Delete(fileMeta.Path);

            return fileMeta;
        }
    }
}
