module.exports = {
  root: true,

  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaVersion: 2018
  },

  env: {
    browser: true
  },

  extends: ['prettier', 'plugin:vue/essential', '@vue/typescript'],

  plugins: ['vue'],

  globals: {
    ga: true,
    __statics: true,
    process: true,
    Capacitor: true,
    chrome: true
  },

  rules: {
    'prefer-promise-reject-errors': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
};
