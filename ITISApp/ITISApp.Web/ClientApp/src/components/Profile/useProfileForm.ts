import { reactive, ref } from '@vue/composition-api';
import User from '@/models/User';

export function useProfileForm(user: User) {
  const isEdit = ref(false);
  const toggleEdit = () => {
    isEdit.value = !isEdit.value;
  };

  const usernameForm = reactive({
    surname: user.surname,
    name: user.name,
    middleName: user.middleName,
    email: user.email,
  });
  const imageUrl = ref(user.imageUrl);

  const cancel = () => {
    usernameForm.name = user.name;
    usernameForm.surname = user.surname;
    usernameForm.middleName = user.middleName;
    usernameForm.email = user.email;
    imageUrl.value = user.imageUrl;
    isEdit.value = false;
  };

  return {
    isEdit,
    toggleEdit,
    usernameForm,
    imageUrl,
    cancel,
  };
}
