import Lesson from '@/models/Lesson';
import { isCssColor } from '@/utils/color';
import { colors } from 'quasar';

export function useCalendarBadge() {
  const badgeClasses = (lesson: Lesson) => {
    const cssColor = isCssColor(lesson.bgcolor);
    return {
      [`text-white bg-${lesson.bgcolor}`]: !cssColor,
    };
  };

  const dotColor = (lesson: Lesson) => {
    return {
      border: `5px solid ${lesson.bgcolor}`,
    };
  };

  const badgeStyles = (event: any, timeStartPos?: Function, timeDurationHeight?: Function) => {
    const s: any = {};
    if (isCssColor(event.bgcolor)) {
      s['background-color'] = event.bgcolor;
      s.color = colors.luminosity(event.bgcolor) > 0.5 ? 'black' : 'white';
    }
    if (timeStartPos) {
      s.top = timeStartPos(event.time) + 'px';
      s.position = 'absolute';
      s.width = '97%';
    }
    if (timeDurationHeight) {
      s.height = timeDurationHeight(event.duration) + 'px';
    }
    s['align-items'] = 'flex-start';
    return s;
  };

  return {
    badgeClasses,
    badgeStyles,
    dotColor,
  };
}
