export default class OptionalSection {
  name: string;
  sectionId: number;
  subjectId: number;
  hasSelectedGroup: boolean;
  isSubject: boolean;
  selectedCourseName: string;
  selectedCourseTeacherName: string;

  constructor(
    name: string,
    subjectId: number,
    sectionId: number,
    hasSelectedGroup: boolean,
    isSubject: boolean,
    selectedCourseName: string,
    selectedCourseTeacherName: string
  ) {
    this.name = name;
    this.subjectId = subjectId;
    this.sectionId = sectionId;
    this.hasSelectedGroup = hasSelectedGroup;
    this.isSubject = isSubject;
    this.selectedCourseName = selectedCourseName;
    this.selectedCourseTeacherName = selectedCourseTeacherName;
  }
}
