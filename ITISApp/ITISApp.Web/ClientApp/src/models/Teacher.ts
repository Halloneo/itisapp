import User from '@/models/User';

export default class Teacher extends User {
  workingExperience: number | undefined;
  isTeacher: boolean;

  constructor(
    id: number,
    login: string,
    name?: string,
    surname?: string,
    middleName?: string,
    email?: string,
    imageUrl?: string,
    workingExperience?: number
  ) {
    super(id, login, name, surname, middleName, email, imageUrl);
    this.workingExperience = workingExperience;
    this.isTeacher = true;
  }
}
