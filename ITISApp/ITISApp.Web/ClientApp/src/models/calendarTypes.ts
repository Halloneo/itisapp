export type calendarViewType = 'day' | 'week' | 'month';

export type periodChangeType = 'previous' | 'next';
