import auth from '@/router/auth';
import select from '@/router/select';
import isLogged from '@/router/isLogged';
import multiguard from 'vue-router-multiguard';

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue'), beforeEnter: auth },
      { path: '/tests', component: () => import('pages/Test.vue'), beforeEnter: auth },
      { path: '/timetable', component: () => import('pages/Timetable.vue'), beforeEnter: auth },
      { path: '/performance', component: () => import('pages/Performance.vue'), beforeEnter: auth },
      { path: '/profile', component: () => import('pages/Profile.vue'), beforeEnter: auth },
      { path: '/login', component: () => import('pages/Login.vue'), beforeEnter: isLogged },
      { path: '/registration', component: () => import('pages/Registration.vue'), beforeEnter: isLogged },
      { path: '/forgotPassword', component: () => import('pages/ForgotPassword.vue'), beforeEnter: isLogged },
      { path: '/resetPassword', component: () => import('pages/ResetPassword.vue'), beforeEnter: isLogged },
      { path: '/lectures', component: () => import('pages/Lectures.vue'), beforeEnter: auth },
      { path: '/timetableUpload', component: () => import('pages/TimetableUpload.vue'), beforeEnter: auth },
    ],
  },
  {
    path: '/',
    component: () => import('layouts/SelectLayout'),
    children: [{ path: '/select', component: () => import('pages/Select.vue'), beforeEnter: multiguard([auth, select]) }],
  },
];

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/Error404.vue'), beforeEnter: auth }],
  });
}

export default routes;
