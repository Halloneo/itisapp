import { store } from '@/store';
import { computed } from '@vue/composition-api';

export default (to: any, from: any, next: any) => {
  const user = computed(() => store.getters['Auth/User']);
  if (user.value.isStudent || user.value.isTeacher) {
    next('/');
  } else {
    next();
  }
};
