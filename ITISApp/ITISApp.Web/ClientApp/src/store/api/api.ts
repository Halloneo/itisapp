import axios from 'axios';
import { AxiosInstance, AxiosResponse } from 'axios';
import { LocalStorage } from 'quasar';
import { Loading } from 'quasar';
import Student from '@/models/Student';
import Teacher from '@/models/Teacher';
import OptionalSection from '@/models/OptionalSection';

export default class Api {
  client: AxiosInstance;
  accessToken: String | null;
  refreshToken: String | null;
  refreshRequest: Promise<AxiosResponse> | null;

  constructor() {
    this.client = axios.create();
    this.accessToken = '';
    this.refreshToken = LocalStorage.getItem('refreshToken') || '';
    this.refreshRequest = null;

    this.client.interceptors.request.use(
      (config) => {
        if (!this.accessToken) {
          return config;
        }

        const newConfig = {
          headers: {},
          ...config,
        };

        newConfig.headers.Authorization = `Bearer ${this.accessToken}`;
        return newConfig;
      },
      (e) => {
        Promise.reject(e);
      }
    );

    this.client.interceptors.response.use(
      (r) => r,
      async (error) => {
        if (!this.refreshToken || error.response.status !== 401 || error.config.retry) {
          throw error;
        }

        if (!this.refreshRequest) {
          this.refreshRequest = this.client.post('/api/auth/RefreshTokens', {
            refreshToken: this.refreshToken,
          });
        }
        Loading.show();
        const { data } = await this.refreshRequest;
        this.accessToken = data.accessToken;
        this.refreshToken = data.refreshToken;
        Loading.hide();
        const newRequest = {
          ...error.config,
          retry: true,
        };

        return this.client(newRequest);
      }
    );
  }

  async uploadTimetable(formData: FormData, callback: (progress: number) => void) {
    return this.client.post('/api/Timetable/GenerateTimetable', formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
      onUploadProgress: (uploadEvent) => {
        const queueProgress = Math.round(uploadEvent.loaded / uploadEvent.total);
        callback(queueProgress);
      },
    });
  }

  async uploadLecture(formData: FormData, callback: (progress: number) => void) {
    return this.client.post('/api/file/upload', formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
      onUploadProgress: (uploadEvent) => {
        const queueProgress = Math.round(uploadEvent.loaded / uploadEvent.total);
        callback(queueProgress);
      },
    });
  }

  async login({ login, password, fingerprint }: { login: string; password: string; fingerprint: string }) {
    try {
      Loading.show();
      const { data } = await this.client.post('/api/auth/login', { login, password, fingerprint });
      this.accessToken = data.data.accessToken;
      this.refreshToken = data.data.refreshToken;
      LocalStorage.set('refreshToken', this.refreshToken);
      Loading.hide();
      return { success: data.success };
    } catch (error) {
      Loading.hide();
      const data = error.response.data;
      return { success: false, message: data.message, parameterName: data.parameterName };
    }
  }

  async register({ login, password, email, fingerprint }: { login: string; password: string; email: string; fingerprint: string }) {
    try {
      Loading.show();
      const { data } = await this.client.post('/api/auth/register', { login, password, email, fingerprint });
      this.accessToken = data.data.accessToken;
      this.refreshToken = data.data.refreshToken;
      LocalStorage.set('refreshToken', this.refreshToken);
      return { success: data.success };
    } catch (error) {
      Loading.hide();
      return { success: false };
    }
  }

  async authenticateWithToken() {
    if (!LocalStorage.has('refreshToken')) {
      throw Error('refreshToken does not exist');
    }
    Loading.show();
    const { data } = await this.client.get(`/api/auth/refresh?refreshToken=${this.refreshToken}`).finally(() => {
      Loading.hide();
    });
    this.accessToken = data.data.accessToken;
    this.refreshToken = data.data.refreshToken;
    LocalStorage.set('refreshToken', this.refreshToken);
  }

  async getSessionUser() {
    const { data } = await this.client.get(`/api/auth/getSessionUser?refreshToken=${this.refreshToken}`);
    return data.data;
  }

  async checkLoginUsed(login: string) {
    const { data } = await this.client.get(`/api/auth/checkLoginUsed?login=${login}`);
    return data.success;
  }

  async checkEmailUsed(email: string) {
    const { data } = await this.client.get(`/api/auth/checkEmailUsed?email=${email}`);
    return data.success;
  }

  async forgotPassword(login: string) {
    const { data } = await this.client.post(`/api/auth/forgotPassword?login=${login}`);
    return data.data;
  }

  async resetPassword(token: string, newPassword: string) {
    const { data } = await this.client.post(`/api/auth/resetPassword?token=${token}&newPassword=${newPassword}`);
    return data;
  }

  async logout() {
    Loading.show();
    await this.client
      .get(`/api/auth/logout?refreshToken=${this.refreshToken}`)
      .catch((e) => console.log(e))
      .finally(() => {
        Loading.hide();
      });
    this.accessToken = null;
    this.refreshToken = null;
    LocalStorage.remove('refreshToken');
  }

  async getAcademicGroups(courseNumber: number) {
    const result = await this.client.get(`/api/profile/getAcademicGroups?courseNumber=${courseNumber}`);
    return result.data;
  }

  async getOptionalSections(studentId: number, courseNumber: number) {
    const result = await this.client.get(`/api/optionalSubject/getOptionalSections?studentId=${studentId}&courseNumber=${courseNumber}`);
    return result.data;
  }

  async getSubjects(sectionId: number) {
    const result = await this.client.get(`/api/optionalSubject/getSubjectsForSection?sectionId=${sectionId}`);
    return result.data;
  }

  async getTeachers(subjectId: number) {
    const result = await this.client.get(`/api/optionalSubject/getTeachersForSubject?subjectId=${subjectId}`);
    return result.data;
  }

  async getSubGroups(teacherId: number, academicGroupId: number) {
    const result = await this.client.get(`/api/optionalSubject/getAvailableGroups?subjectTeacherId=${teacherId}&academicGroupId=${academicGroupId}`);
    return result.data;
  }

  async addStudentToOptionalGroup(studentId: number, subGroup: number, section: OptionalSection) {
    const result = await this.client.post(
      `/api/optionalSubject/addStudentToOptionalGroup?studentId=${studentId}&optionalGroupId=${subGroup}`,
      section
    );
    return result.data;
  }

  async createStudent(student: Student) {
    const result = await this.client.post(`/api/profile/createStudent`, student);
    await this.authenticateWithToken();
    return result.data;
  }

  async createTeacher(teacher: Teacher) {
    const result = await this.client.post(`/api/profile/createTeacher`, teacher);
    await this.authenticateWithToken();
    return result.data;
  }

  async updateStudent(student: Student) {
    const result = await this.client.post(`/api/profile/updateStudent`, student);
    return result.data;
  }

  async updateTeacher(teacher: Teacher) {
    const result = await this.client.post(`/api/profile/updateTeacher`, teacher);
    return result.data;
  }
}

export const api = new Api();
