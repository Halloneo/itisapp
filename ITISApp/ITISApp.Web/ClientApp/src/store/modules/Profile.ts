import { Getters, Mutations, Actions, Module, Context } from 'vuex-smart-module';
import { Store } from 'vuex';
import { api } from '@/store/api/api';
import AcademicGroup from '@/models/AcademicGroup';
import Student from '@/models/Student';
import { Auth } from '@/store/modules/Auth';
import User from '@/models/User';
import Teacher from '@/models/Teacher';

class ProfileState {
  academicGroups: AcademicGroup[] = [];
}

class ProfileGetters extends Getters<ProfileState> {
  get AcademicGroups() {
    return this.state.academicGroups;
  }
}

class ProfileMutations extends Mutations<ProfileState> {
  setAcademicGroups(academicGroups: AcademicGroup[]) {
    this.state.academicGroups = academicGroups;
  }
}

class ProfileActions extends Actions<ProfileState, ProfileGetters, ProfileMutations, ProfileActions> {
  auth: Context<typeof Auth> | undefined;

  $init(store: Store<any>): void {
    this.auth = Auth.context(store);
  }

  async getAcademicGroups(courseNumber: number) {
    const result = await api.getAcademicGroups(courseNumber);
    if (result.success) {
      this.mutations.setAcademicGroups(result.data);
    }
    return result;
  }

  async updateStudent(student: Student) {
    const result = await api.updateStudent(student);
    if (result.success) {
      this.auth!.mutations.setUser(student);
    }
    return result;
  }

  async updateTeacher(teacher: Teacher) {
    const result = await api.updateTeacher(teacher);
    if (result.success) {
      this.auth!.mutations.setUser(teacher);
    }
    return result;
  }

  async createStudent(student: Student) {
    const result = await api.createStudent(student);
    if (result.success) {
      let user = (await api.getSessionUser()) as User;
      this.auth!.mutations.setUser(user);
    }
    return result;
  }

  async createTeacher(teacher: Teacher) {
    const result = await api.createTeacher(teacher);
    if (result.success) {
      let user = (await api.getSessionUser()) as User;
      this.auth!.mutations.setUser(user);
    }
    return result;
  }
}

export const Profile = new Module({
  state: ProfileState,
  getters: ProfileGetters,
  mutations: ProfileMutations,
  actions: ProfileActions,
});
