import { Getters, Mutations, Actions, Module } from 'vuex-smart-module';
import { api } from '@/store/api/api';
import Lesson from '@/models/Lesson';
import { colors } from '@/utils/color';
class TimetableState {
  lessons: Lesson[] = [];
}

class TimetableGetters extends Getters<TimetableState> {
  get Lessons() {
    return this.state.lessons;
  }
}

class TimetableMutations extends Mutations<TimetableState> {
  setLessons(lessons: Lesson[]) {
    lessons.forEach((x) => {
      const random = Math.floor(Math.random() * colors.length);
      x.bgcolor = colors[random];
    });
    this.state.lessons = lessons;
  }
}

class TimetableActions extends Actions<TimetableState, TimetableGetters, TimetableMutations, TimetableActions> {
  async getLessons() {
    const result = await api.client.get('/api/timetable/getTimetable');
    if (result.data.success) {
      this.commit('setLessons', result.data.data);
    }
  }
}

export const Timetable = new Module({
  state: TimetableState,
  getters: TimetableGetters,
  mutations: TimetableMutations,
  actions: TimetableActions,
});
