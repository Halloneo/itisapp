export const isCssColor = (color) => {
  return !!color && !!color.match(/^(#|(rgb|hsl)a?\()/);
};

export const colors = ['red', 'orange', 'green', 'teal', '#607d8b', 'blue', 'purple'];
