import { useActions } from '@u3u/vue-hooks';

const { checkEmailUsed } = useActions('Auth', ['checkEmailUsed']);

const emailExistValidator = async (email: string) => {
  if (email) return await checkEmailUsed(email);
  return false;
};

export const emailExist = {
  $validator: emailExistValidator,
  $message: 'Данный Email уже используется',
};
