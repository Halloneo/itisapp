﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITISApp.Domain.Core.Authorization.Handlers;
using ITISApp.Domain.Core.Authorization.Requirements;
using ITISApp.Domain.Core.Common;
using ITISApp.Domain.Core.Entities.Authorization;
using ITISApp.Domain.Core.Helpers;
using ITISApp.Domain.Core.Interfaces;
using ITISApp.Domain.Core.Services;
using ITISApp.Domain.Students.Services;
using ITISApp.Domain.Teachers.Services;
using ITISApp.Domain.Teachers.Services.Implementation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace ITISApp.Web
{
    public static class StartupExtensions
    {
        private static List<Role> _registeredRoles;
        private static List<Permission> _registeredPermissions;
        private static Dictionary<string, List<string>> _registeredRolePermissions;

        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = JwtOptions.Issuer,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtOptions.Key)),
                    ValidateIssuerSigningKey = true
                };
            });
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddSingleton<PasswordCheckService>();
            services.AddScoped<TokenService>();

            services.AddScoped<IDataStore, DataStore>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<UserDomainService>();
            services.AddScoped<EmailService>();
            services.AddScoped<TimetableService>();
            services.AddScoped<ScheduleImportService>();
            services.AddScoped<LessonsGenerationService>();
            services.AddTransient<IAuthorizationHandler, RoleHandler>();
            services.AddScoped<IAuthorizationHandler, PermissionHandler>();
        }

        public static void AddPoliciesAuthorization(this IServiceCollection services)
        {
            _registeredRoles = new List<Role>
            {
                new Role(RoleCodesHelper.Admin, "Администратор"),
                new Role(RoleCodesHelper.Student, "Студент"),
                new Role(RoleCodesHelper.Teacher, "Преподаватель")
            };

            _registeredPermissions = new List<Permission>
            {
                new Permission(PermissionCodesHelper.UploadTimetable, "Загрузка файла с расписанием")
            };

            _registeredRolePermissions = new Dictionary<string, List<string>>
            {
                {
                    RoleCodesHelper.Admin, new List<string>
                    {
                        PermissionCodesHelper.UploadTimetable
                    }
                }
            };

            services.AddAuthorization(options =>
            {
                _registeredRoles.ForEach(options.RegisterRole);
                _registeredPermissions.ForEach(options.RegisterPermission);
            });
        }

        public static void CheckRegisteredRolesForExisting(this IServiceProvider serviceProvider)
        {
            var dataStore = serviceProvider.GetService<IDataStore>();
            dataStore.InTransaction(() =>
            {
                var roleCodes = _registeredRoles.Select(x => x.Code);
                var existingRoles = dataStore.GetAll<Role>()
                    .Where(x => roleCodes.Contains(x.Code))
                    .ToList();

                _registeredRoles
                    .Where(x => !existingRoles.Contains(x))
                    .ToList()
                    .ForEach(role => dataStore.Create(role));
            });
        }

        public static void CheckRegisteredPermissionsForExisting(this IServiceProvider serviceProvider)
        {
            var dataStore = serviceProvider.GetService<IDataStore>();
            dataStore.InTransaction(() =>
            {
                var permissionCodes = _registeredPermissions.Select(x => x.Code);
                var existingPermissions = dataStore.GetAll<Permission>()
                    .Where(x => permissionCodes.Contains(x.Code))
                    .ToList();

                _registeredPermissions
                    .Where(x => !existingPermissions.Contains(x))
                    .ToList()
                    .ForEach(permission => dataStore.Create(permission));
            });
        }

        public static void CheckRegisteredRolePermissionsForExisting(this IServiceProvider serviceProvider)
        {
            var dataStore = serviceProvider.GetService<IDataStore>();
            dataStore.InTransaction(() =>
            {
                var registeredRoles = _registeredRolePermissions.Select(x => x.Key);
                var actualRoles = dataStore.GetAll<Role>()
                    .Where(x => registeredRoles.Contains(x.Code))
                    .ToList();

                foreach (var role in actualRoles)
                {
                    var permissionsForRole = _registeredRolePermissions[role.Code];
                    var actualPermissions = dataStore.GetAll<RolePermission>()
                        .Where(x => x.Role.Code == role.Code)
                        .Select(x => x.Permission.Code)
                        .ToList();

                    var permissionsForDelete = actualPermissions
                        .Where(x => !permissionsForRole.Contains(x));
                    var permissions = dataStore.GetAll<RolePermission>()
                        .Where(x => x.Role.Code == role.Code)
                        .Where(x => permissionsForDelete.Contains(x.Permission.Code))
                        .ToList();
                    permissions.ForEach(rolePermission => dataStore.Delete(rolePermission));

                    var notExistingRolePermissions = permissionsForRole
                        .Where(x => !actualPermissions.Contains(x));
                    var rolePermissions = dataStore.GetAll<Permission>()
                        .Where(x => notExistingRolePermissions.Contains(x.Code))
                        .ToList();
                    rolePermissions.ForEach(permission => dataStore.Create(new RolePermission(role, permission)));
                }
            });
        }

        private static void RegisterRole(this AuthorizationOptions options, Role role)
        {
            options.AddPolicy(role.Code,
                builder => { builder.Requirements.Add(new RoleRequirement(role.Code)); });
        }

        private static void RegisterPermission(this AuthorizationOptions options, Permission permission)
        {
            options.AddPolicy(permission.Code,
                builder => { builder.Requirements.Add(new PermissionRequirement(permission.Code)); });
        }
    }
}